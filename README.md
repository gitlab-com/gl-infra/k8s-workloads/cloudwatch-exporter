# Cloudwatch exporter

This repository is mirrored on https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/

Kubernetes Workload configurations for Cloudwatch Exporter

## Storage

:warning: **WARNING** :warning:

The following are _NOT_ allowed this repository:
* Files that create Kubernetes Objects of type `Secret`
* Files that contain secrets in plain text


